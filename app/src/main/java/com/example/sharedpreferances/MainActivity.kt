package com.example.sharedpreferances

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init() {
        sharedPreferences = getSharedPreferences("user_information", MODE_PRIVATE)

        val email = sharedPreferences.getString("email", "")
        val firstname = sharedPreferences.getString("firstname", "")
        val lastname = sharedPreferences.getString("lastname", "")
        val age = sharedPreferences.getString("age", "")
        val address = sharedPreferences.getString("address", "")

        firstName.setText(firstname)
        lastName.setText(lastname)
        eMail.setText(email)
        Age.setText(age)
        Address.setText(address)

        save_button.setOnClickListener {
            if (eMail.text.toString().isNotEmpty() && firstName.text.toString().isNotEmpty() && lastName.text.toString().isNotEmpty() && Age.text.toString().isNotEmpty() && Address.text.toString().isNotEmpty()) {
                save_information()
                Toast.makeText(this, "Information Successfully Saved", Toast.LENGTH_SHORT).show()

            }
            else
            {
                Toast.makeText(applicationContext, "Please fill all fields", Toast.LENGTH_SHORT).show()
            }
        }
    }


    fun save_information() {
        val firstname = firstName.text.toString()
        val lastname = lastName.text.toString()
        val email = eMail.text.toString()
        val age = Age.text.toString()
        val address = Address.text.toString()

        val editor = sharedPreferences.edit()
        editor.putString("email", email)
        editor.putString("firstname", firstname)
        editor.putString("lastname", lastname)
        editor.putString("age", age)
        editor.putString("address", address)
        editor.apply()
    }

}